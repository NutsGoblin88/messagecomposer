/*--------------------------------------------------
---
---Interface    :   IKeyPadObserver
---Author       :   Wiedmer Noël
---Version      :   1.0
---Date         :   15.03.2022
---
--------------------------------------------------*/
#ifndef IKEYPADPAD_ONCE
#define IKEYPADPAD_ONCE
#include <string>

class IKeyPadObserver
{
public:
    virtual void checkButton(const char* _row) = 0;
};

#endif