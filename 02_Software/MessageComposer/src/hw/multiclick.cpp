#include "multiclick.h"

Multiclick Multiclick::theMulticlick;

/**
* @brief Multiclick constructor
*/
Multiclick::Multiclick()
{

}

/**
* @brief Multiclick destructor
*/
Multiclick::~Multiclick()
{

}

/**
* @brief Initialize Multiclcick
* Set the initial State to ST_INIT, set the event target to this object and
* set the event to do not delete
*/
void Multiclick::init()
{
	state = ST_INIT;
	event.setTarget(this);
	event.setDnd(1);
	evTimer.setTarget(this);
	evTimer.setDnd(1);

	printk("Multiclick init done \n");
}
/**
* @brief get the Multiclick only instance
*
* @return Multiclick instance
*/
Multiclick* Multiclick::getInstance() 
{
    return &theMulticlick;
}

/**
* @brief start state machine
* push the evInitial event to start the state machine
*/
void Multiclick::startBehaviour()
{
	event.setId(evInitial);
	XF::getInstance()->pushEvent(&event);
	printk("init event pushed m \n");
}

/**
* @brief virutal method from IButtonObserver
* when executed push the evPressed event
* @param id from button and active row from keypad
*/
void Multiclick::pressed(const char* row, int id)
{ 	
    event.setId(evPressed);
	event.setTarget(this);
	XF::getInstance()->pushEvent(&event);
	this->row = row;
	this->id = id;
}

/**
* @brief virutal method from IButtonObserver
*
* @param id from button and active row from keypad
*/
void Multiclick::released(const char* row, int id)
{ 
    
}

/**
* @brief subcribe object to IMulticlickObserver
*
* @param IMulticlickObserver object
*/
void Multiclick::subscribe(IMulticlickObserver * multiclickObserver)
{
	observerList.push_back(multiclickObserver);
}

/**
* @brief notify all objects
* this method notifys all object which are subcribed to the IMulticlickObserver
* @param active row, button id and clickstate of the Button
*/
void Multiclick::notify(const char* _row, int _id, int _clickstate)
{
	for (auto observer : observerList)
    {
        observer->pressed(_row,_id,_clickstate);
    }
}

/**
* @brief state machine
* Incoming events will get proccessed in this method
* when evPressed has been pushed start a timer and switch to ST_0 
* if timer run out send active state as clickstate
* if evPressed pushed again change to next state and cancel and restart the timer 
* @param e Event from XF
*
* @return false if event not used, true if evend used
*/
bool Multiclick::processEvent(Event *e)
{
	MULTICLICKSTATES oldstate;

	oldstate = this->state;

	switch (this->state)
	{
	case ST_INIT:
		if(e->getId() == evInitial)
		{
			this->state = ST_WAIT;
		}
		break;
	case ST_WAIT:
		if(e->getId() == evPressed)
		{
			this->state = ST_0;
		}
		break;
	case ST_0:
		if(e->getId() == evTimeout)
		{
			this->state = ST_WAIT;
		}
		if(e->getId() == evPressed)
		{
			this->state = ST_1;
		}
		break;
	case ST_1:
		if(e->getId() == evTimeout)
		{
			this->state = ST_WAIT;
		}
		if(e->getId() == evPressed)
		{
			this->state = ST_2;
		}
		break;
	case ST_2:
		if(e->getId() == evTimeout)
		{
			this->state = ST_WAIT;
		}
		if(e->getId() == evPressed)
		{
			this->state = ST_3;
		}
		break;
	case ST_3:
		if(e->getId() == evTimeout)
		{
			this->state = ST_WAIT;
		}
		if(e->getId() == evPressed)
		{
			this->state = ST_0;
		}
		break;
	}
	if(oldstate != this->state)
	{
		switch (oldstate)
		{
			case ST_INIT:
			{
				break;
			}
			case ST_WAIT:
			{
				XF::getInstance()->unscheduleTM(&evTimer);
				break;
			}
			case ST_0:
			{
				XF::getInstance()->unscheduleTM(&evTimer);
				break;
			}
			case ST_1: 
			{
				XF::getInstance()->unscheduleTM(&evTimer);
				break;
			}
			case ST_2:
			{
				XF::getInstance()->unscheduleTM(&evTimer);
				break;
			}
			case ST_3:
			{
				XF::getInstance()->unscheduleTM(&evTimer);
				break;
			}
		}
		int t = 800;
		switch (this->state)
		{
			case ST_INIT:
			{
				printk("init multi \n");
				break;
			}
			case ST_WAIT:
			{	
				if(oldstate == ST_0)
				{
					notify(row,id,0);
				}
				else if(oldstate == ST_1)
				{
					notify(row,id,1);
				}
				else if(oldstate == ST_2)
				{
					notify(row,id,2);
				}
				else if(oldstate == ST_3)
				{
					notify(row,id,3);
				}
				break;
			}
			case ST_0:
			{
				evTimer.setId(evTimeout);
				evTimer.setTarget(this);
				evTimer.setDelay(t);
				XF::getInstance()->pushEvent(&evTimer);				
				break;
			}
			case ST_1:
			{				
				evTimer.setId(evTimeout);
				evTimer.setTarget(this);
				evTimer.setDelay(t);
				XF::getInstance()->pushEvent(&evTimer);	
				break;
			}
			case ST_2:
			{
				evTimer.setId(evTimeout);
				evTimer.setTarget(this);
				evTimer.setDelay(t);
				XF::getInstance()->pushEvent(&evTimer);	
				break;
			}
			case ST_3:
			{
				evTimer.setId(evTimeout);
				evTimer.setTarget(this);
				evTimer.setDelay(t);
				XF::getInstance()->pushEvent(&evTimer);	
				break;
			}
		}
		return true;
	}
	return false;
}
