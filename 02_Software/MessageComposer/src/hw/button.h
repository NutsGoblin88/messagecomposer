/*--------------------------------------------------
---
---Class        :   Button
---Author       :   Wiedmer Noël
---Version      :   1.1
---Date         :   15.03.2022
---Description  :   reactive class which informs if a button
---                 is pressed or released
--------------------------------------------------*/
#ifndef BUTTON_ONCE
#define BUTTON_ONCE

#include "../xf/xf.h"
#include "ikeypadobserver.h"
#include "ibuttonobserver.h"
#include <stdint.h>
#include <zephyr.h>
#include <drivers/gpio.h>
#include <stdlib.h>
#include <list>
#include <string>

class Button : public IReactive , public IKeyPadObserver
{
public:
    Button();
    ~Button();
    void initHW(const char* _row, int _id, int pin, const char* port);
    bool processEvent(Event* event);
    void startBehaviour();
    void subscribe(IButtonObserver * buttonObserver);    
    typedef enum POLLSTATE
    {
        ST_INITIAL,
        ST_WAIT,
        ST_POLL
    } POLLSTATE;        //enum with all states for the state machine
    typedef enum PINSTATE
    {
        LOW,
        HIGH
    } PINSTATE;         //enum for state of the button

private: 
    Event pollTm;                   //object for timeout event
    Event ev;                       //object for other events 
    POLLSTATE state;                //active poll state
    PINSTATE pinState;              //active pin state
    PINSTATE oldPinState;           //last pin state
    const struct device* driver;    //port for gpio input
    int pin;
    const char* row;
    int id;
    typedef enum buttonEvID {
		evInitial
        ,evDefault
        ,evTimeout
        ,evPoll
	} keypadEvID;                   //enum for all events
    std::list <IButtonObserver*> observerList; //list with all objects subscribed to observer
    void checkButton(const char* _row);
    void notify(PINSTATE ps);
};
#endif