/*--------------------------------------------------
---
---Interface    :   IButtonObserver
---Author       :   Wiedmer Noël
---Version      :   1.0
---Date         :   15.03.2022
---
--------------------------------------------------*/
#ifndef IBUTTONOBSERVER_ONCE
#define IBUTTONOBSERVER_ONCE
#include <string>

class IButtonObserver
{
public:
    virtual void pressed(const char* row, int id) = 0;
    virtual void released(const char* row, int id) = 0;
};

#endif