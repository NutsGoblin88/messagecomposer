/*--------------------------------------------------
---
---Class        :   Multiclick
---Author       :   Wiedmer Noël
---Version      :   1.2
---Date         :   15.03.2022
---Description  :   reactive class and returns how many time a button has been pressed
---					
--------------------------------------------------*/
#ifndef MULTICLICK_H
#define MULTICLICK_H

#include "../xf/xf.h"
#include "../xf/event.h"
#include "../xf/IReactive.h"
#include "ibuttonObserver.h"
#include "imulticlickobserver.h"
#include <list>

class Multiclick : public IReactive, public IButtonObserver 
{
public:
	void init();
	static Multiclick* getInstance();
	void startBehaviour();
	bool processEvent(Event* e);
	typedef enum MULTICLICKSTATES {
		ST_INIT, 
		ST_WAIT, 
		ST_0, 
		ST_1, 
		ST_2, 
		ST_3	
	} MULTICLICKSTATES; 	//enum for all States
	void subscribe(IMulticlickObserver * multiclickObserver);

private:
	Multiclick();
	~Multiclick();
	static Multiclick theMulticlick;	//only instance of the multiclick
	MULTICLICKSTATES state;				//active state of multiclick
	typedef enum multiclickEvID {
		evInitial,
 		evPressed,
 		evTimeout
	} multiclickEvID;	//enum for all Events 
	Event event; 		//Object for regular events
	Event evTimer; 		//Object only for the timeout events	
	const char* row;
	int id;
	std::list <IMulticlickObserver*> observerList;	//list with all objects subscribed to observer
	void pressed(const char* row, int id) override;	
    void released(const char* row, int id) override;
	void notify(const char* _row, int _id, int _clickstate);
};
#endif
