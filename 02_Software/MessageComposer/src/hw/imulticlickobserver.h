/*--------------------------------------------------
---
---Interface    :   IMulticlickObserver
---Author       :   Wiedmer Noël
---Version      :   1.0
---Date         :   15.03.2022
---
--------------------------------------------------*/
#ifndef IMULTICLICK_H
#define IMULTICLICK_H
#include <string>

class IMulticlickObserver
{
public:
    virtual void pressed(const char* _row, int _id, int _clickstate) = 0;
};

#endif