#include "button.h"
#include "../xf/xf.h"
#include "../factory/factory.h"

/**
* @brief Button constructor
* set active state to ST_INIT, set target to this class and set event to
* do not delete. set pollTm id to evTimeout and add a timer with 50ms delay
* set pinstate to low
*/
Button::Button()
{
    pollTm.setDnd(true);
    pollTm.setId(evTimeout);
    pollTm.setTarget(this);
    pollTm.setDelay(50);

    ev.setDnd(true);
    ev.setTarget(this);

    state = ST_INITIAL;
    pinState = LOW;
    oldPinState = LOW;
}

/**
* @brief Button destructor
*/
Button::~Button()
{

}

/**
* @brief initialize Button
* bind driver to the port of the nucleo
* configure the pin of the nucleo to input so it can read incomming signals
* get pinstate of the pin
* @param row, id, pin and port from the Button
*/
void Button::initHW(const char* _row, int _id, int p, const char* port)
{
    id = _id;
    row = _row;
    pin = p;
    
    int ret;
    driver = const_cast<device*> (device_get_binding(port));
    ret = gpio_pin_configure(driver,pin,GPIO_INPUT | GPIO_ACTIVE_LOW | GPIO_PULL_DOWN | GPIO_INT_DEBOUNCE);
    pinState =  (PINSTATE) gpio_pin_get(driver,pin);
    oldPinState = pinState;
    //printk("button init done\n");
}

/**
* @brief start state machine
* push the evInitial event to start the state machine
*/
void Button::startBehaviour()
{
    ev.setId(evInitial);
    Factory::xf()->pushEvent(&ev);
    //printk("init event pushed b \n");
}

/**
* @brief virtual method from IKeypadObserver
* compare the row of the button to the active row from the nucleo
* if same row the push evPoll event
* @param avtice row
*/
void Button::checkButton(const char* _row)
{
    if(row == _row)
    {
        ev.setId(evPoll);
        Factory::xf()->pushEvent(&ev);
    }
}

/**
* @brief subcribe object to IButtonObserver
*
* @param IButtonObserver object
*/
void Button::subscribe(IButtonObserver * buttonObserver)
{
    observerList.push_back(buttonObserver);
}

/**
* @brief notify all objects
* this method notifys all object which are subcribed to the IKeyPadObserver
* if the pinstate is HIGH then execute pressed methode
* if the pinstate is LOW then execute released methode
* @param pinstate
*/
void Button::notify(PINSTATE ps)
{
    for (auto observer : observerList)
    {
        if(ps == HIGH)
        {
            observer->pressed(row, id);  
            //printk("pressed \n");       
        }
        else
        {
            observer->released(row, id);
            //printk("released \n");       

        }
    }
}

/**
* @brief state machine
* Incoming events will get proccessed in this method
* This state machine will check if the input pin is HIGH or LOW
* @param e Event from XF
*
* @return false if event not used, true if evend used
*/
bool Button::processEvent(Event* event)
{
    bool processed = false;
    POLLSTATE oldstate = state;
    switch (state)
    {
        case ST_INITIAL:
        if (event->getId()==evInitial)
        {
            state = ST_WAIT;

        }
        break;
        case ST_WAIT:
        if (event->getId() == evPoll)
        {
            state = ST_POLL;
        }
        break;
        case ST_POLL:
        if (event->getId() == evDefault)
        {
            state = ST_WAIT;
        }
        break;
        default:
        break;
    }
    if (state != oldstate)
    {
        switch (state)
        {
            case ST_INITIAL:
            printk("State Initial Button\n");
            break;
            case ST_WAIT:
            //printk("State Wait\n");
            break;
            case ST_POLL:
            //printk("State Poll\n");
            pinState = (PINSTATE) gpio_pin_get(driver,pin);
            if (pinState == HIGH && oldPinState == LOW)
            {
                notify(LOW);
            }
            if (pinState == LOW && oldPinState == HIGH)
            {
                notify(HIGH);
            }
            if (pinState ==  oldPinState)
            {
                //no change
            }
            oldPinState = pinState;
            ev.setId(evDefault);
            Factory::xf()->pushEvent(&ev);
            break;
            default:
            break;
        }
        processed = true;
    }
    return processed;
}
