#include "keypad.h"

/**
* @brief Keypad constructor
* 
* @param pinA,pinB,pinC,pinD and Port of the Nucleo
*/
KeyPad::KeyPad(int pA, int pB, int pC, int pD, const char* port) :
	pinA(pA,port), pinB(pB,port), pinC(pC,port), pinD(pD,port)
{

}

/**
* @brief Keypad destructor
*/
KeyPad::~KeyPad()
{

}

/**
* @brief initialize keypad
* set active state to ST_INIT, set target to this class and set event to
* do not delete
* set Pin pinA-D from the Nucleo off and initialize the Pin
*/
void KeyPad::init()
{
	state = ST_INIT;
	ev.setTarget(this);
	ev.setDnd(1);
	
	pinA.setInitialOff();
    pinB.setInitialOff();
    pinC.setInitialOff();
    pinD.setInitialOff();
    pinA.initHW();
    pinB.initHW();
    pinC.initHW();
    pinD.initHW();
	printk("keypad init done\n");
}

/**
* @brief start state machine
* push the evInitial event to start the state machine
*/
void KeyPad::startBehaviour()
{
	ev.setId(evInitial);
	XF::getInstance()->pushEvent(&ev);
	printk("init event pushed k \n");
}

/**
* @brief subcribe object to IKeyPadObserver
*
* @param IKeyPadObserver object
*/
void KeyPad::subscribe(IKeyPadObserver * keyPadObserver)
{
	keyPadObserverList.push_back(keyPadObserver);
}

/**
* @brief notify all objects
* this method notifys all object which are subcribed to the IKeyPadObserver
* @param active row
*/
void KeyPad::notify(const char* row)
{
	for(auto observer : keyPadObserverList)
    {
		observer->checkButton(row);
	}
}

/**
* @brief state machine
* Incoming events will get proccessed in this method
* This state machine will power the pins pinA-pinD of the nucleo 
* at a 10 ms interval
* @param e Event from XF
*
* @return false if event not used, true if evend used
*/
bool KeyPad::processEvent(Event *e)
{
	KEYPADSTATES oldstate;

	oldstate = this->state;

	switch (this->state)
	{
		case ST_INIT:
		{
			if(e->getId() == evInitial)
			{
				this->state = ST_WAIT;
			}
			break;
		}
		case ST_WAIT:
		{
			if(e->getId() == evTm10)
			{
				this->state = ST_A;
				
			}
			break;
		}
		case ST_A:
		{
			if(e->getId() == evTm10)
			{
				this->state = ST_B;

			}
			break;
		}
		case ST_B:
		{			
			if(e->getId() == evTm10)
			{
				this->state = ST_C;
					
			}
			break;
		}
		case ST_C:
		{
			if(e->getId() == evTm10)
			{
				this->state = ST_D;
						
			}
			break;
		}
		case ST_D:
		{
			if(e->getId() == evTm10)
			{
				this->state = ST_A;
						
			}
			break;
		}
	}
	if(oldstate != this->state)
	{
		switch (oldstate)
		{
			case ST_INIT:
			{
				break;
			}
			case ST_WAIT:
			{
				break;
			}
			case ST_A:
			{
				pinA.off();
				//printk("pinA off\n");
				break;
			}
			case ST_B:
			{
				pinB.off();
				//printk("pinB off\n");				
				break;
			}
			case ST_C:
			{
				pinC.off();
				//printk("pinC off\n");				
				break;
			}
			case ST_D:
			{
				pinD.off();
				//printk("pinD off\n");				
				break;
			}
		}

		int t = 10;
		switch (this->state)
		{
			case ST_INIT:
			{
				printk("init\n");
				break;
			}
			case ST_WAIT:
			{
				ev.setId(evTm10);
				ev.setTarget(this);
				ev.setDelay(t);
				XF::getInstance()->pushEvent(&ev);
				break;
			}
			case ST_A:
			{
				pinA.on();
				notify("A");
				//printk("pinA on\n");
				ev.setId(evTm10);
				ev.setTarget(this);
				ev.setDelay(t);
				XF::getInstance()->pushEvent(&ev);
				break;
			}
			case ST_B:
			{
				pinB.on();
				notify("B");
				//printk("pinB on\n");
				ev.setId(evTm10);
				ev.setTarget(this);
				ev.setDelay(t);
				XF::getInstance()->pushEvent(&ev);				
				break;
			}
			case ST_C:
			{
				pinC.on();
				notify("C");
				//printk("pinC on\n");
				ev.setId(evTm10);
				ev.setTarget(this);
				ev.setDelay(t);
				XF::getInstance()->pushEvent(&ev);
				break;
			}
			case ST_D:
			{
				pinD.on();
				notify("D");
				//printk("pinD on\n");
				ev.setId(evTm10);
				ev.setTarget(this);
				ev.setDelay(t);
				XF::getInstance()->pushEvent(&ev);
				break;
			}
		}
		return true;
	}
	return false;
}
