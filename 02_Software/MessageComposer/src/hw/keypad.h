/*--------------------------------------------------
---
---Class        :   Keypad
---Author       :   Wiedmer Noël
---Version      :   1.2
---Date         :   15.03.2022
---Description  :   reactive class which activates pinA,pinB,pinC and pinD at a 10ms
---					interval
--------------------------------------------------*/
#ifndef KEYPAD_H
#define KEYPAD_H

#include "../xf/xf.h"
#include "../xf/event.h"
#include "../xf/IReactive.h"
#include "../gpio/gpo.h"
#include "ikeypadobserver.h"
#include <stdint.h>
#include <list>
#include <string>

class KeyPad : public IReactive
{
public:
	KeyPad(int pA, int pB, int pC, int pD, const char* port);
	~KeyPad();
	void init();
	void startBehaviour();
	bool processEvent(Event* e);
	typedef enum KEYPADSTATES {
		ST_INIT, 
		ST_WAIT, 
		ST_A, 
		ST_B, 
		ST_C, 
		ST_D
	} KEYPADSTATES;			//enum for all states
	void subscribe(IKeyPadObserver * keyPadObserver);
	

private:
	KEYPADSTATES state;		//active state of keypad
	typedef enum keypadEvID {
		evInitial
		,evTm10
	} keypadEvID;			//enum for all events

	std::list <IKeyPadObserver*> keyPadObserverList; //list with all objects subscribed to observer
	Event ev;				// event ev
	GPO pinA;               // General Purpose Output
    GPO pinB;               // General Purpose Output
    GPO pinC;               // General Purpose Output
    GPO pinD;               // General Purpose Output
	void notify(const char* row);
};
#endif
