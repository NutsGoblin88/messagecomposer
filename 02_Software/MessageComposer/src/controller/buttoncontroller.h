/*--------------------------------------------------
---
---Class        :   ButtonController
---Author       :   Wiedmer Noël
---Version      :   1.4
---Date         :   15.03.2022
---Description  :   controlls what happens when a button is pressed
---                 
--------------------------------------------------*/
#ifndef BUTTONCONTROLLER_ONCE
#define BUTTONCONTROLLER_ONCE

#include "../xf/xf.h"
#include "../gpio/gpi.h"
#include "../hw/imulticlickobserver.h"
#include "../fona/ifonaobserver.h"
#include <drivers/gpio.h>
#include <list>
#include <zephyr.h>
#include <string>
#include <vector>
#include <algorithm>

class Model;
class Fona;

class ButtonController : public IMulticlickObserver, public IFonaObserver
{
public:
    static ButtonController* getInstance();
    void init();
    void maxtrixLayout();

private: 
    ButtonController();
    ~ButtonController();
    static ButtonController theButtonController;    //only instance of the buttoncontroller
    vector<vector<vector<char>>> characters;        //3D Vector Array for all characters
    vector<const char*> rows = {"A","B","C","D"};   //Array of Rows
    vector<string> buttons = {"A0","A1","A2","A3","B0","B1","B2","B3","C0","C1","C2","C3","D0","D1","D2","D3"}; //Array of Buttons
    vector<char> strSMS;    //Vector to save written sms
    int x,y,z;
    Model * model;          //pointer to Model class
    Fona * fona;            //pointer ot fona class
    bool fonaStarted;
    void pressed(const char* _row, int _id, int _clickstate) override;
    string getStrSMS();
    void addCharToSMS(char s);
    void deleteCharFromSMS();
    void deleteSMS();
    void sendSMS();
    void onResponse(char* message) override;
};
#endif