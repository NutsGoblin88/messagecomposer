#include "buttonController.h"
#include "../factory/factory.h"

ButtonController ButtonController::theButtonController;

/**
* @brief ButtonController constructor
*/
ButtonController::ButtonController()
{        
    
}

/**
* @brief ButtonController destructor
*/
ButtonController::~ButtonController()
{
    
}

/**
* @brief initialize ButtonController
* connect Fona and Model with Buttoncontroller
* get all character assignments and save them in characters array
* send AT Command to fona, to check if the modem is reachable
*/
void ButtonController::init()
{   
    model = Factory::theModel();
    fona = Factory::theFona();
    characters = model->getCharacters();
    fonaStarted = false;
    fona->send("AT+CMGS=?");
    printk("ButtonController init done \n");
}

/**
* @brief get the ButtonController only instance
*
* @return ButtonController instance
*/
ButtonController* ButtonController::getInstance() 
{
    return &theButtonController;
}

/**
* @brief virtual method from IMulticlickObserver
* first get Buttonnumber from Button and also transform row into an index
* if the buttonnumber is special execute a method in the switch case block
* else the character will be read from the characters array and added to the
* strSMS vector
* @param row, id and clickstate from Button
*/
void ButtonController::pressed(const char* _row, int _id, int _clickstate)
{ 
    int buttonNr = distance(buttons.begin(), find(buttons.begin(), buttons.end(), _row + to_string(_id))) + 1; //get Index of Button out of Button Array 
    x = distance(rows.begin(), find(rows.begin(), rows.end(), _row)); //get Index of rows to compare with the CharachterMatrix
    y = _id; //Button ID
    z = _clickstate; //Clickstate of Button
    maxtrixLayout();

    //check if special Button is pressed
    switch (buttonNr)
    {
        case 4:
            if(model->getStrSMS().size() > 0)
            {
                deleteCharFromSMS();
                printk("%s \n", getStrSMS().c_str());
            }
            else
                printk("FAILIURE: \"empty string\" \n");
            break;
        case 8:
            if(model->getStrSMS().size() > 0)
                deleteSMS();
            else
                printk("FAILIURE: \"empty string\" \n");
            break;   
        case 12:
            if(fonaStarted)
            {
                if(model->getStrSMS().size() > 0)
                    sendSMS();
                else
                    printk("FAILIURE: \"No SMS written\" \n");
            }
            else
            {
                printk("FAILURE: Fona Modem is not started \n");
            }
            break;     
        default:
            addCharToSMS(characters[x][y][z]);            
            printk("%s \n", getStrSMS().c_str());
            break;
    }
    
}

/**
* @brief print matrixLayout
* print character assignment to buttons
*/
void ButtonController::maxtrixLayout()
{
    string maxtrixLayout =  "|abc1|def2|ghi3 |Del | \n"
                            "|jkl4|mno5|pqr6 |Clr | \n"
                            "|stu7|vwx8|yz_9 |Snd | \n"
                            "|.,*/|0000|space|DDDD| \n";

    printk("%s \n", maxtrixLayout.c_str());                          
}

/**
* @brief add one character to SMS array
*/
void ButtonController::addCharToSMS(char s)
{
    strSMS.push_back(s);
    model->setStrSMS(strSMS);
}

/**
* @brief delete one character from SMS array
*/
void ButtonController::deleteCharFromSMS()
{
    strSMS.pop_back();
    model->setStrSMS(strSMS);
}

/**
* @brief delete all characters from SMS array
*/
void ButtonController::deleteSMS()
{
    strSMS.clear();
    model->setStrSMS(strSMS);
}

/**
* @brief get SMS from Model
*
* @return SMS as string
*/
string ButtonController::getStrSMS()
{
    string _output;
    for(unsigned int i = 0; i < model->getStrSMS().size();i++)
    {
        _output += model->getStrSMS()[i];
    }
    return _output;
}

/**
* @brief send SMS
* send AT Command for sending SMS using Fona method
* use 10ms sleep before sending SMS
* execute deleteSMS method
*/
void ButtonController::sendSMS()
{    
    fona->send("AT+CMGS=\"0764681812\"");    
    k_sleep(K_MSEC(10));
    fona->send(getStrSMS()+"\x1A"); 
    
    deleteSMS();    
}

/**
* @brief virtual method from IFonaObserver
* compare incomming message if == 0 then print 
* response for user 
* if response OK check first if modem already is started
* @param message from fona
*/
void ButtonController::onResponse(char* message)
{
    if(strcmp(message, "PB DONE\r\n")==0)
    {
        printk("Fona started \n");
        fonaStarted = true;
    }
    if(strcmp(message, "ERROR\r\n")==0)
    {
        printk("SMS couldn't send correctly \n");
    }
    if(strcmp(message, "AT+CMGS=? \r\n")==0)
    {
        printk("SMS couldn't send correctly \n");
    }
    if(strcmp(message, "OK\r\n")==0)
    {
        if(fonaStarted)
        {
            printk("Message has been sent successfully \n");
        }
        fonaStarted = true;
    }
}


