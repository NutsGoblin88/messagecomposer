#include "factory.h"

KeyPad Factory::_theKeypad(5,4,12,8, "GPIOA"); 
Uart Factory::_theUart("UART_1",115200);
Button Factory::_buttonA1;
Button Factory::_buttonA2;
Button Factory::_buttonA3;
Button Factory::_buttonA4;
Button Factory::_buttonB1;
Button Factory::_buttonB2;
Button Factory::_buttonB3;
Button Factory::_buttonB4;
Button Factory::_buttonC1;
Button Factory::_buttonC2;
Button Factory::_buttonC3;
Button Factory::_buttonC4;
Button Factory::_buttonD1;
Button Factory::_buttonD2;
Button Factory::_buttonD3;
Button Factory::_buttonD4;

/**
* @brief Factory constructor
*/
Factory::Factory()
{

}

/**
* @brief Factory destructor
*/
Factory::~Factory()
{

}

/**
* @brief initialize all components
*/
void Factory::init()
{
    printk("----start init---- \n");
    xf()->init();
    theKeyPad()->init();
    theMulticlick()->init();
    theUart()->initHW();
    theUart()->enableRXInterrupt();
    theFona()->init();
    theModel()->init();
    theButtonController()->init();

    buttonA1()->initHW("A", 0, 0,"GPIOB");
    buttonA2()->initHW("A", 1, 1,"GPIOB");
    buttonA3()->initHW("A", 2, 5,"GPIOB");
    buttonA4()->initHW("A", 3, 4,"GPIOB");
    buttonB1()->initHW("B", 0, 0,"GPIOB");
    buttonB2()->initHW("B", 1, 1,"GPIOB");
    buttonB3()->initHW("B", 2, 5,"GPIOB");
    buttonB4()->initHW("B", 3, 4,"GPIOB");
    buttonC1()->initHW("C", 0, 0,"GPIOB");
    buttonC2()->initHW("C", 1, 1,"GPIOB");
    buttonC3()->initHW("C", 2, 5,"GPIOB");
    buttonC4()->initHW("C", 3, 4,"GPIOB");
    buttonD1()->initHW("D", 0, 0,"GPIOB");
    buttonD2()->initHW("D", 1, 1,"GPIOB");
    buttonD3()->initHW("D", 2, 5,"GPIOB");
    buttonD4()->initHW("D", 3, 4,"GPIOB");
    printk("----init done----\n");
}

/**
* @brief build connections
*/
void Factory::build()
{
    printk("----start build---- \n");
    theKeyPad()->subscribe(buttonA1());
    theKeyPad()->subscribe(buttonA2());
    theKeyPad()->subscribe(buttonA3());
    theKeyPad()->subscribe(buttonA4());
    theKeyPad()->subscribe(buttonB1());
    theKeyPad()->subscribe(buttonB2());
    theKeyPad()->subscribe(buttonB3());
    theKeyPad()->subscribe(buttonB4());
    theKeyPad()->subscribe(buttonC1());
    theKeyPad()->subscribe(buttonC2());
    theKeyPad()->subscribe(buttonC3());
    theKeyPad()->subscribe(buttonC4());
    theKeyPad()->subscribe(buttonD1());
    theKeyPad()->subscribe(buttonD2());
    theKeyPad()->subscribe(buttonD3());
    theKeyPad()->subscribe(buttonD4());
    buttonA1()->subscribe(theMulticlick());
    buttonA2()->subscribe(theMulticlick());
    buttonA3()->subscribe(theMulticlick());
    buttonA4()->subscribe(theMulticlick());
    buttonB1()->subscribe(theMulticlick());
    buttonB2()->subscribe(theMulticlick());
    buttonB3()->subscribe(theMulticlick());
    buttonB4()->subscribe(theMulticlick());
    buttonC1()->subscribe(theMulticlick());
    buttonC2()->subscribe(theMulticlick());
    buttonC3()->subscribe(theMulticlick());
    buttonC4()->subscribe(theMulticlick());
    buttonD1()->subscribe(theMulticlick());
    buttonD2()->subscribe(theMulticlick());
    buttonD3()->subscribe(theMulticlick());
    buttonD4()->subscribe(theMulticlick());
    theMulticlick()->subscribe(theButtonController());
    theUart()->subscribe(theFona());
    theFona()->subscribe(theButtonController());
    printk("----build done---- \n");
}

/**
* @brief start behaviours of the reactive components
*/
void Factory::start()
{
    printk("----start startBehaviour----\n");
    theKeyPad()->startBehaviour();
    theMulticlick()->startBehaviour();
    buttonA1()->startBehaviour();
    buttonA2()->startBehaviour();
    buttonA3()->startBehaviour();
    buttonA4()->startBehaviour();
    buttonB1()->startBehaviour();
    buttonB2()->startBehaviour();
    buttonB3()->startBehaviour();
    buttonB4()->startBehaviour();
    buttonC1()->startBehaviour();
    buttonC2()->startBehaviour();
    buttonC3()->startBehaviour();
    buttonC4()->startBehaviour();
    buttonD1()->startBehaviour();
    buttonD2()->startBehaviour();
    buttonD3()->startBehaviour();
    buttonD4()->startBehaviour();
    printk("----startBehaviour done----\n");
}
