/*--------------------------------------------------
---
---Class        :   Factory
---Author       :   Wiedmer Noël
---Version      :   1.2
---Date         :   16.03.2022
---Description  :   initializes all Classes from this project
---                 
--------------------------------------------------*/
#ifndef FACTORY_ONCE
#define FACTORY_ONCE

#include "../xf/xf.h" 
#include "../hw/button.h"
#include "../hw/keypad.h"
#include "../hw/multiclick.h"
#include "../hw/uart.h"
#include "../controller/buttoncontroller.h"
#include "../model/model.h"
#include "../fona/fona.h"

class Factory
{
public:
    static void init();
    static void build();
    static void start();

    static XF* xf(){return XF::getInstance();}
    static KeyPad* theKeyPad(){return &_theKeypad;}
    static Uart* theUart(){return &_theUart;}
    static Fona* theFona(){return Fona::getInstance();}
    static ButtonController* theButtonController(){return ButtonController::getInstance();}
    static Multiclick* theMulticlick(){return Multiclick::getInstance();}
    static Model* theModel(){return Model::getInstance();}

    static Button* buttonA1(){return &_buttonA1;}
    static Button* buttonA2(){return &_buttonA2;}
    static Button* buttonA3(){return &_buttonA3;}
    static Button* buttonA4(){return &_buttonA4;}
    static Button* buttonB1(){return &_buttonB1;}
    static Button* buttonB2(){return &_buttonB2;}
    static Button* buttonB3(){return &_buttonB3;}
    static Button* buttonB4(){return &_buttonB4;}
    static Button* buttonC1(){return &_buttonC1;}
    static Button* buttonC2(){return &_buttonC2;}
    static Button* buttonC3(){return &_buttonC3;}
    static Button* buttonC4(){return &_buttonC4;}
    static Button* buttonD1(){return &_buttonD1;}
    static Button* buttonD2(){return &_buttonD2;}
    static Button* buttonD3(){return &_buttonD3;}
    static Button* buttonD4(){return &_buttonD4;}

private:
    Factory();
    ~Factory();
    static KeyPad _theKeypad;
    static Uart _theUart;

    static Button _buttonA1;
    static Button _buttonA2;
    static Button _buttonA3;
    static Button _buttonA4;
    static Button _buttonB1;
    static Button _buttonB2;
    static Button _buttonB3;
    static Button _buttonB4;
    static Button _buttonC1;
    static Button _buttonC2;
    static Button _buttonC3;
    static Button _buttonC4;
    static Button _buttonD1;
    static Button _buttonD2;
    static Button _buttonD3;
    static Button _buttonD4;
};

#endif