/*--------------------------------------------------
---
---Class        :   Model
---Author       :   Wiedmer Noël
---Version      :   1.1
---Date         :   15.03.2022
---Description  :   saves all data from program
---
--------------------------------------------------*/
#ifndef MODEL_H
#define MODEL_H

#include <zephyr.h>
#include <vector>
#include <string>

using namespace std;

class Model
{
    public:
        void init();
        static Model* getInstance();
        vector<vector<vector<char>>> getCharacters();
        vector<char> getStrSMS();
        void setStrSMS(vector<char> v);

    private:
        Model();
        ~Model();
        static Model theModel;                      //onyl instance of the model
        vector<vector<vector<char>>> characters;    //3D Array of all characters assigned to a button
        vector<char> strSMS;                        //written SMS saved as vector
};

#endif