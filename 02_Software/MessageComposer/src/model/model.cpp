#include "model.h"

#define HEIGHT 4
#define WIDTH 4
#define DEPTH 4

Model Model::theModel;

Model::Model()
{
}

Model::~Model()
{
}

Model * Model::getInstance()
{
    return &theModel;
}

void Model::init()
{
    // Set up sizes. (HEIGHT x WIDTH x DEPTH)
    characters.resize(HEIGHT);
    for (int i = 0; i < HEIGHT; ++i) {
        characters[i].resize(WIDTH);
        for (int j = 0; j < WIDTH; ++j)
            characters[i][j].resize(DEPTH);
    }

    characters[0][0][0] = 'a'; //A1
    characters[0][0][1] = 'b'; //A1
    characters[0][0][2] = 'c'; //A1
    characters[0][0][3] = '1'; //A1
    characters[0][1][0] = 'd'; //A2
    characters[0][1][1] = 'e'; //A2
    characters[0][1][2] = 'f'; //A2
    characters[0][1][3] = '2'; //A2
    characters[0][2][0] = 'g'; //A3
    characters[0][2][1] = 'h'; //A3
    characters[0][2][2] = 'i'; //A3
    characters[0][2][3] = '3'; //A3
    characters[0][3][0] = 'A'; //A4
    characters[0][3][1] = 'A'; //A4
    characters[0][3][2] = 'A'; //A4
    characters[0][3][3] = 'A'; //A4

    characters[1][0][0] = 'j'; //B1
    characters[1][0][1] = 'k'; //B1
    characters[1][0][2] = 'l'; //B1
    characters[1][0][3] = '4'; //B1
    characters[1][1][0] = 'm'; //B2
    characters[1][1][1] = 'n'; //B2
    characters[1][1][2] = 'o'; //B2
    characters[1][1][3] = '5'; //B2
    characters[1][2][0] = 'p'; //B3
    characters[1][2][1] = 'q'; //B3
    characters[1][2][2] = 'r'; //B3
    characters[1][2][3] = '6'; //B3
    characters[1][3][0] = 'B'; //B4
    characters[1][3][1] = 'B'; //B4
    characters[1][3][2] = 'B'; //B4
    characters[1][3][3] = 'B'; //B4

    characters[2][0][0] = 's'; //C1
    characters[2][0][1] = 't'; //C1
    characters[2][0][2] = 'u'; //C1
    characters[2][0][3] = '7'; //C1
    characters[2][1][0] = 'v'; //C2
    characters[2][1][1] = 'w'; //C2
    characters[2][1][2] = 'x'; //C2
    characters[2][1][3] = '8'; //C2
    characters[2][2][0] = 'y'; //C3
    characters[2][2][1] = 'z'; //C3
    characters[2][2][2] = '_'; //C3
    characters[2][2][3] = '9'; //C3
    characters[2][3][0] = 'C'; //C4
    characters[2][3][1] = 'C'; //C4
    characters[2][3][2] = 'C'; //C4
    characters[2][3][3] = 'C'; //C4

    characters[3][0][0] = '.'; //D1
    characters[3][0][1] = ','; //D1
    characters[3][0][2] = '*'; //D1
    characters[3][0][3] = '/'; //D1
    characters[3][1][0] = '0'; //D2
    characters[3][1][1] = '0'; //D2
    characters[3][1][2] = '0'; //D2
    characters[3][1][3] = '0'; //D2
    characters[3][2][0] = ' '; //D3
    characters[3][2][1] = ' '; //D3
    characters[3][2][2] = ' '; //D3
    characters[3][2][3] = ' '; //D3
    characters[3][3][0] = 'D'; //D4
    characters[3][3][1] = 'D'; //D4
    characters[3][3][2] = 'D'; //D4
    characters[3][3][3] = 'D'; //D4

    printk("Model init done \n");
}

vector<vector<vector<char>>> Model::getCharacters()
{
    return characters;
}

vector<char> Model::getStrSMS()
{
    return strSMS;
}

void Model::setStrSMS(vector<char> v)
{
    strSMS = v;
}
