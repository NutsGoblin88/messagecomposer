/*--------------------------------------------------
---
---Interface    :   IFonaObserver
---Author       :   Wiedmer Noël
---Version      :   1.0
---Date         :   15.03.2022
---
--------------------------------------------------*/
#ifndef IFONAOBSERVER_H
#define IFONAOBSERVER_H
#include <string>

class IFonaObserver
{
public:
    virtual void onResponse(char* message) = 0;
};

#endif