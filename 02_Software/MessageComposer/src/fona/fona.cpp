#include "fona.h"
#include "..\factory\factory.h"

Fona Fona::theFona;

/**
* @brief Fona constructor
*/
Fona::Fona()
{

}

/**
* @brief Fona destructor
*/
Fona::~Fona()
{
}

/**
* @brief initialize Fona
* connect Uart with Fona Uart pointer
*/
void Fona::init()
{
    uart = Factory::theUart();
}

/**
* @brief get the Fona only instance
*
* @return Fona instance
*/
Fona* Fona::getInstance()
{
    return &theFona;
}

/**
* @brief send message to uart
* add escape sequence to the message
* execute uartSend method 
* @param message
*/
void Fona::send(string message){
        message += "\r\n";
        uart->uartSend(message.c_str());
}

/**
* @brief subcribe object to IFonaObserver
*
* @param IFonaObserver object
*/
void Fona::subscribe(IFonaObserver* subscriber)
{
    subscribers.push_back(subscriber);
}

/**
* @brief notify all objects
* this method notifys all object which are subcribed to the IKeyPadObserver
*/
void Fona::notify()
{
    for(auto observer : subscribers)
    {
		observer->onResponse((char*)data);
	}
}

/**
* @brief virtual method from IUARTObserver
* get message from kernel message queue and
* save it in data attribute
* @param message from message queue
*/
void Fona::onMessage(k_msgq* messages){  
    k_msgq_get(messages, &data, K_NO_WAIT);
    if(!(strncmp(data, "\r\n",2)==0)){
        notify();
    }
}