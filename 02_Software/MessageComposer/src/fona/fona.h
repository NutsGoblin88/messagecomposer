/*--------------------------------------------------
---
---Class        :   Fona
---Author       :   Wiedmer Noël
---Version      :   1.2
---Date         :   15.03.2022
---Description  :   comunicates with the uart class
---                 
--------------------------------------------------*/
#ifndef FONA_ONCE
#define FONA_ONCE

#include "../hw/uart.h"
#include "ifonaobserver.h"
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

class Fona : public Uart::IUARTObserver
{
    public:
        static Fona* getInstance();
        void init();
        void subscribe(IFonaObserver* subscriber);
        void send(string command);
    private:
        Fona();
        ~Fona();
        void notify();
        void onMessage(k_msgq* messages) override;
        static Fona theFona;                    //only instance of the Fona
        Uart* uart;                             //pointer to the Uart instance
        vector<IFonaObserver*> subscribers;     //list with all objects subscribed to observer
        char data[MAXDATASIZE];                 //char array with MAXDATASIZE declared in uart class
};

#endif