#include <zephyr.h>
#include "factory/factory.h"
#include "xf/xf.h"

void main(void)
{

    Factory::init();
    Factory::build();
    Factory::start();
    Factory::theButtonController()->maxtrixLayout();
    try
    {
        while(1)
        {
            k_sleep(K_MSEC(4));
            //printk("loooop\n");            
            Factory::xf()->execute();
        }
    }
    catch(const std::exception& e)
    {
        printk("Error occurred: %s \n",e.what());
        exit(EXIT_FAILURE);
    }        
}